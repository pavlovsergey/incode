import express from 'express';
import bodyParser from 'body-parser';
import path from 'path';
import cors from 'cors';

import * as Model from './server/app/model/DataBaseUtils';
import * as userController from './server/app/controllers/users';
import * as contentController from './server/app/controllers/content';

import passport from 'passport';
import './server/app/controllers/passport';

const app = express ();

// Set up connection of database
Model.setUpConnection ();

app.use (cors ());
app.use (bodyParser.json ());
app.use (bodyParser.urlencoded ({extended: true}));

app.use (passport.initialize ());
app.use (passport.session ());

app.use ('/', express.static (__dirname + '/build/'));
app.use ('/static', express.static (__dirname + '/build/'));
app.get ('/verification', userController.verificationPage);

app.post ('/signin', userController.authorization);
app.post ('/signup', userController.createUser);
app.post ('/verification', userController.verification);

app.post (
  '/dashboard',
  passport.authenticate ('jwt', {session: false}),
  contentController.getContent
);
app.post (
  '/changecategory',
  passport.authenticate ('jwt', {session: false}),
  contentController.changeCategory
);
app.post (
  '/addexpenses',
  passport.authenticate ('jwt', {session: false}),
  contentController.addExpenses
);
app.post (
  '/findexpenses',
  passport.authenticate ('jwt', {session: false}),
  contentController.findExpenses
);

var port = process.env.PORT || 3001;

app.listen (port, () => {
  console.log ('privet', port);
});
