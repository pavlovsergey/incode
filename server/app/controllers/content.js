import * as Model from '../model/DataBaseUtils';

// ##############################
// // // ***Category***
// #############################
////////////////////////////////////////////////////////////////////////////////////////////////////////////

export function changeCategory (req, res) {
  const content = req.body;

  Model.changeCategory (req.user.username, content).then (updateCat => {
    if (updateCat.nModified == 0) {
      res.send ({updateCat: false});
    } else {
      res.send ({updateCat: true});
    }
  });
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////

// ##############################
// // // ***Expenses***
// #############################

////////////////////////////////////////////////////////////////////////////////////////////////////////////

export function addExpenses (req, res) {
  const expenses = req.body;

  Model.addExpenses (req.user.id, expenses).then (NewExpenses => {
    res.send ({id: NewExpenses._id});
  });
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////

export function findExpenses (req, res) {
  const expensesTime = req.body;

  Model.findExpenses (
    req.user.id,
    expensesTime.expensesTimeStart,
    expensesTime.expensesTimeEnd
  ).then (expenses => {
    res.send (expenses);
  });
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////

// ##############################
// // // ***Content***
// #############################

////////////////////////////////////////////////////////////////////////////////////////////////////////////
export function getContent (req, res) {
  if (typeof req.body !== 'undefined') {
    const data = req.body;

    Model.getContent (req.user.id).then (user => {
      if (!user) {
        res.send ({
          token: false,
        });
      } else {
        let content = JSON.parse (user[0].content);

        if (user[0].icon) {
          content.icon = user[0].icon;
        } else {
          content.icon = '';
        }
        ///////////////////////////////////////////////////////////////////////////////////////////////////
        if (!user[0].content) {
          content.expenses = {};
          res.send ({content: content});
        } else {
          Model.getContentExpenses (req.user.id).then (expenses => {
            if (expenses) {
              content.expenses = {};

              expenses.forEach (function (item) {
                content.expenses[item._id] = {
                  description: item.description,
                  date: item.date,
                  category: item.category,
                  value: item.value,
                  idUser: item.idUser,
                };
              });
              res.send ({content: content});
            } else {
              content.expenses = {};

              res.send ({
                content: content,
              });
            }
          });
        }
      }
    });
  }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
