import * as Model from '../model/DataBaseUtils';
import path from 'path';
import jwt from 'jsonwebtoken';

////////////////////////////////////////////////////////////////////////////////////////////////////////////

export function createUser (req, res) {
  const data = req.body;

  Model.createUser (data)
    .then (data => {
      const resp = `http://localhost:3000/verification?name=${data.username}&code=${data.code}`;

      res.send (resp);
    })
    .catch (function (error) {
      if (error.code == 11000) {
        return res
          .status (409)
          .send ('Name already in use. Please choose another one.');
      } else {
        return res.status (500).send (error.message);
      }
    });
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////

export function verification (req, res) {
  const data = req.body;
  const token = jwt.sign ({email: req.query.name}, 'your_jwt_secret');
  Model.verification (req.query.name, req.query.code, token).then (data => {
    if (data.nModified == 0) {
      res.redirect ('/');
    } else {
      res.send ({token: token});
    }
  });
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////

export function authorization (req, res) {
  const data = req.body;

  Model.authorization (data).then (resp => {
    if (resp.length == 0) {
      res.send (
        404,
        'There is no password with this name or password. Please verify  the username and password'
      );
    } else {
      if (!resp[0].verification) {
        res.redirect ('/');
      } else {
        res.send ({token: resp[0].token});
      }
    }
  });
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////

export function verificationPage (req, res) {
  res.sendFile (path.join (__dirname, '../../../build/index.html'));
}
