import passport from 'passport';
let JwtStrategy = require ('passport-jwt').Strategy;
let ExtractJwt = require ('passport-jwt').ExtractJwt;
import mongoose from 'mongoose';
import '../model/models/users';
const users = mongoose.model ('users');

var opts = {};
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken ();
opts.secretOrKey = 'your_jwt_secret';

passport.use (
  new JwtStrategy (opts, function (jwtPayload, done) {
    users.findOne ({username: jwtPayload.email}, function (err, user) {
      if (err) {
        done (err, false);
      }
      if (user) {
        return done (null, user);
      } else {
        return done (null, false);
        // or you could create a new account
      }
    });
  })
);
