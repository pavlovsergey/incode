import mongoose from 'mongoose';
import './models/users';
import './models/expenses';
import mlabUrl from './url';
var url =
  process.env.MONGOLAB_URI || mlabUrl || `mongodb://localhost:27017/expense`;
//connect  DataBase//////////////////////////////////////////////////////////////////////////////////////////

export function setUpConnection () {
  mongoose.connect (
    url,
    {
      useNewUrlParser: true,
    },
    err => {
      if (err) return console.log (err);
      console.log ('DataBase true');
    }
  );
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////

const users = mongoose.model ('users');
const expenses = mongoose.model ('expenses');

// ##############################
// // // ***Users***
// #############################

////////////////////////////////////////////////////////////////////////////////////////////////////////////

export function createUser (data) {
  const user = new users ({
    username: data.email,
    password: data.password,
    token: '',
    verification: false,
    content: '{"filterContent": { "orderCategory": [] },"category": {}}',
    icon: '',

    code: Date.now (),
  });
  return user.save ();
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////

export function authorization (data) {
  return users.find ({username: data.email, password: data.password});
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////

export function verification (username, code, jwt) {
  return users.update (
    {username: username, code: code},
    {$set: {verification: true, token: jwt}}
  );
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////

// ##############################
// // // ***Users END***
// #############################

// ##############################
// // // ***Category***
// #############################

////////////////////////////////////////////////////////////////////////////////////////////////////////////

export function getContent (id) {
  return users.find ({_id: id});
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////

export function changeCategory (username, content) {
  return users.update (
    {username: username},
    {$set: {content: JSON.stringify (content)}}
  );
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////

// ##############################
// // // ***Category END***
// #############################

// ##############################
// // // ***Expenses***
// #############################

////////////////////////////////////////////////////////////////////////////////////////////////////////////

export function addExpenses (idUser, expens) {
  const newExpenses = new expenses ({
    description: expens.description,
    date: expens.date,
    category: expens.category,
    value: expens.value,
    idUser: idUser,
  });
  return newExpenses.save ();
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////

export function getContentExpenses (id) {
  return expenses.find ({idUser: id}).sort ({date: -1}).limit (20);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////

export function findExpenses (idUser, startDate, endDate) {
  return expenses.find (
    {idUser: idUser, date: {$gte: startDate, $lte: endDate}},
    {category: 1, value: 1, _id: 0}
  );
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////

// ##############################
// // // ***Expenses END***
// #############################
