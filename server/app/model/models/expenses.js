import mongoose from "mongoose";


const ExpensesSchema = new mongoose.Schema({

    description: {
        type: String,
    },
    date: {
        type: Number,
        required: true
    },

    category: {
        type: String
    },
    value: {
        type: String
    },
    idUser: {
        type: String,
        required: true
    },
});

mongoose.model('expenses', ExpensesSchema);
