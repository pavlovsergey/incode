import mongoose from "mongoose";


const UserSchema = new mongoose.Schema({
    username: {
        type: String,
        unique: true,
        required: true
    },
    password: {
        type: String,
        required: true
    },

    token: {
        type: String
    },

    verification: {
        type: Boolean,
        required: true
    },
    content: {
        type: String
    },
    code: {
        type: String,
        required: true
    },
    icon: {
        type: String
    },
});
mongoose.model('users', UserSchema);
