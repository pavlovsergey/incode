import {CHANGE_CATEGORY, ADD_EXPENSES, FILTER_EXPENSES} from '../constans';
import axios from 'axios';

// ##############################
// // // ***category***
// #############################

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

function changeCategory (content) {
  return dispatch => {
    axios.defaults.headers.common['Authorization'] =
      'Bearer ' + window.localStorage.getItem ('jwt');

    if (window.localStorage.getItem ('jwt')) {
      axios
        .post ('http://localhost:3001/changecategory', {...content})
        .then (function (response) {
          if (response.data.updateCat) {
            dispatch ({
              type: CHANGE_CATEGORY,
              payload: {
                content: content,
              },
            });
          }
        })
        .catch (function (error) {
          window.localStorage.removeItem ('jwt');
          document.location.reload (true);
        });
    }
  };
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

export function AddCategory () {
  const id = Date.now ();

  return (dispatch, getState) => {
    const state = getState ();
    let content = state.content;

    content.filterContent.orderCategory = content.filterContent.orderCategory.concat (
      [id]
    );
    content.category[id] = {
      name: 'New category',
      expenses: 0,
      child: [],
      parentCat: '',
    };

    dispatch (changeCategory ({...content}));
  };
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

export function AddParent (id, parentId) {
  return (dispatch, getState) => {
    const state = getState ();

    const deleteOrderCategory = state.content.filterContent.orderCategory.filter (
      itemId => {
        if (itemId !== id) return true;
        return false;
      }
    );

    const category = state.content.category;
    const parentCategory = category[parentId];
    parentCategory.child.push (id);

    category[parentId].child = parentCategory.child;
    category[id].parentCat = parentId;

    const content = {
      category: {...category},
      filterContent: {orderCategory: [...deleteOrderCategory]},
    };

    dispatch (changeCategory (content));
  };
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

export function deleteParent (id, parentId) {
  return (dispatch, getState) => {
    const state = getState ();

    let addOrderCategory = state.content.filterContent.orderCategory;
    addOrderCategory.push (id);

    const category = state.content.category;
    const parentCategory = category[parentId];
    const childIndex = parentCategory.child.indexOf (id);

    category[parentId].child.splice (childIndex, 1);
    category[id].parentCat = '';

    const content = {
      category: {...category},
      filterContent: {orderCategory: [...addOrderCategory]},
    };

    dispatch (changeCategory (content));
  };
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

export function deleteCategory (id, parentId) {
  return (dispatch, getState) => {
    const state = getState ();
    if (parentId) {
      let content = state.content;

      content = DeleteChuild (content, id);

      const category = content.category;
      const parentCategory = content.category[parentId];
      const childIndex = parentCategory.child.indexOf (id);
      parentCategory.child.splice (childIndex, 1);

      category[parentId].child = parentCategory.child;

      content = {
        ...content,
        category: {...category},
      };
      dispatch (changeCategory (content));
    } else {
      let content = state.content;

      const deleteCategory = {...DeleteChuild (content, id).category};
      const deleteOrderCategory = content.filterContent.orderCategory.filter (
        itemId => {
          if (itemId !== id) return true;
          return false;
        }
      );

      content = {
        category: {...deleteCategory},
        filterContent: {orderCategory: [...deleteOrderCategory]},
      };

      dispatch (changeCategory (content));
    }
  };
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

function DeleteChuild (content, id) {
  if (content.category[id].child.length > 0) {
    content.category[id].child.forEach (idChuild => {
      if (content.category[idChuild].child.length > 0)
        DeleteChuild (content, idChuild);
      delete content.category[idChuild];
    });
  }
  delete content.category[id];
  return content;
}

export function saveNameCategory (newName, id) {
  return (dispatch, getState) => {
    const state = getState ();

    let content = state.content;
    content.category[id].name = newName;

    dispatch (changeCategory (content));
  };
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

export function ChangeOrderCategoryUp (id, parentId) {
  return (dispatch, getState) => {
    const state = getState ();
    let content = state.content;
    if (parentId) {
      let parentCategory = state.content.category[parentId];

      const childIndex = parentCategory.child.indexOf (id);

      if (childIndex !== 0) {
        parentCategory.child.splice (childIndex, 1);
        parentCategory.child.splice (childIndex - 1, 0, id);

        const category = state.content.category;
        category[parentId].child = parentCategory.child;

        content = {
          ...content,
          category: {...category},
        };
        dispatch (changeCategory (content));
      }
    } else {
      let orderCategory = state.content.filterContent.orderCategory;
      const index = orderCategory.indexOf (id);

      if (index !== 0) {
        orderCategory.splice (index, 1);
        orderCategory.splice (index - 1, 0, id);

        content = {
          ...content,
          filterContent: {orderCategory: [...orderCategory]},
        };
        dispatch (changeCategory (content));
      }
    }
  };
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

export function ChangeOrderCategoryDown (id, parentId) {
  return (dispatch, getState) => {
    const state = getState ();
    let content = state.content;
    if (parentId) {
      let parentCategory = state.content.category[parentId];
      const childIndex = parentCategory.child.indexOf (id);
      if (childIndex !== parentCategory.child.length - 1) {
        parentCategory.child.splice (childIndex, 1);
        parentCategory.child.splice (childIndex + 1, 0, id);

        const category = state.content.category;
        category[parentId].child = parentCategory.child;

        content = {
          ...content,
          category: {...category},
        };
        dispatch (changeCategory (content));
      }
    } else {
      let orderCategory = state.content.filterContent.orderCategory;
      const index = orderCategory.indexOf (id);

      if (index !== orderCategory.length - 1) {
        orderCategory.splice (index, 1);
        orderCategory.splice (index + 1, 0, id);

        content = {
          ...content,
          filterContent: {orderCategory: [...orderCategory]},
        };

        dispatch (changeCategory (content));
      }
    }
  };
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

// ##############################
// // // ***category END***
// #############################

// ##############################
// // // ***Expenses***
// #############################

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

export function AddExpenses (category, description, value) {
  return dispatch => {
    const expensesSend = {
      date: Date.now (),
      description: description,
      category: category,
      value: value,
    };

    axios.defaults.headers.common['Authorization'] =
      'Bearer ' + window.localStorage.getItem ('jwt');

    if (window.localStorage.getItem ('jwt')) {
      axios
        .post ('http://localhost:3001/addexpenses', expensesSend)
        .then (function (response) {
          if (response.data.id) {
            const id = response.data.id;
            let expenses = {};
            expenses[id] = expensesSend;

            dispatch ({
              type: ADD_EXPENSES,
              payload: {expenses: {...expenses}},
            });
          }
        })
        .catch (function (error) {
          window.localStorage.removeItem ('jwt');
          document.location.reload (true);
        });
    }
  };
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

export function filterExpenses (expensesTimeStart, expensesTimeEnd) {
  return (dispatch, getState) => {
    axios.defaults.headers.common['Authorization'] =
      'Bearer ' + window.localStorage.getItem ('jwt');

    if (window.localStorage.getItem ('jwt')) {
      axios
        .post ('http://localhost:3001/findexpenses', {
          expensesTimeStart,
          expensesTimeEnd,
        })
        .then (function (response) {
          if (response.data) {
            const state = getState ();
            const category = state.content.category;

            for (var item in category) {
              category[item].expenses = 0;
            }

            response.data.forEach (expenses => {
              if (+expenses.value) {
                if (category[expenses.category]) {
                  category[expenses.category].expenses += +expenses.value;
                }
              }
            });

            dispatch ({
              type: FILTER_EXPENSES,
              payload: {category: {...category}},
            });
          }
        })
        .catch (function (error) {
          window.localStorage.removeItem ('jwt');
          document.location.reload (true);
        });
    }
  };
}
// ##############################
// // // ***Expenses End***
// #############################
