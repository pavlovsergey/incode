import {LOAD_START, LOAD_FAIL, LOAD_SUCCESS} from '../constans';

import axios from 'axios';

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

export function signIn (email, password) {
  return dispatch => {
    axios.defaults.headers.common['Authorization'] =
      'Bearer ' + window.localStorage.getItem ('jwt');
    axios
      .post ('http://localhost:3001/signin', {
        email: email,
        password: password,
      })
      .then (response => {
        if (response.data.validEmailPassword) {
          dispatch ({type: LOAD_FAIL});
        } else {
          if (response.data.token) {
            window.localStorage.setItem ('jwt', response.data.token);
            dispatch (load ());
          }
        }
      })
      .catch (function (error) {
        window.localStorage.removeItem ('jwt');
        document.location.reload (true);
      });
  };
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

export function addUser (email, password) {
  axios.defaults.headers.common['Authorization'] =
    'Bearer ' + window.localStorage.getItem ('jwt');
  return dispatch => {
    axios
      .post ('http://localhost:3001/signup', {
        email: email,
        password: password,
      })
      .then (function (response) {
        console.log (response.data);
      })
      .catch (function (error) {
        window.localStorage.removeItem ('jwt');
        document.location.reload (true);
      });
  };
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

export function verification (email, code) {
  return dispatch => {
    axios.defaults.headers.common['Authorization'] =
      'Bearer ' + window.localStorage.getItem ('jwt');
    axios
      .post (`http://localhost:3001/verification?name=${email}&code=${code}`, {
        email: email,
        code: code,
      })
      .then (function (response) {
        if (response.data) {
          if (response.data.token) {
            window.localStorage.setItem ('jwt', response.data.token);
            dispatch (load ());
          }
        }
      })
      .catch (function (error) {
        window.localStorage.removeItem ('jwt');
        document.location.reload (true);
      });
  };
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

export function load () {
  return dispatch => {
    axios.defaults.headers.common['Authorization'] =
      'Bearer ' + window.localStorage.getItem ('jwt');
    dispatch ({type: LOAD_START});
    if (window.localStorage.getItem ('jwt')) {
      axios
        .post ('http://localhost:3001/dashboard', {
          jwt: window.localStorage.getItem ('jwt'),
        })
        .then (function (response) {
          dispatch ({
            type: LOAD_SUCCESS,
            payload: {content: response.data.content},
          });
        })
        .catch (function (error) {
          window.localStorage.removeItem ('jwt');
          dispatch ({type: LOAD_FAIL});
          document.location.reload (true);
        });
    }
  };
}
