import axios from 'axios';

export function axs (query, sendObject) {
  axios.defaults.headers.common['Authorization'] =
    'Bearer ' + window.localStorage.getItem ('jwt');

  return axios.post (query, sendObject);
}
