export function dayNow (dateNow = new Date ()) {
  const dateMonth = dateNow.getMonth ();
  const dateYear = dateNow.getFullYear ();
  const dateSt = dateNow.getDate ();

  const startday = new Date (dateYear, dateMonth, dateSt, 0, 0, 0, 0);
  const endDay = startday.getTime () + 86400000 - 1;
  return {
    startDay: startday.getTime (),
    endDay: endDay,
    month: dateMonth,
    year: dateYear,
    dayNumber: dateSt,
    day: startday,
  };
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

export function moveBackTime (type, start, end) {
  switch (type) {
    case 'month':
      const day = dayNow (new Date (start));

      const startMonth = new Date (day.year, day.month - 1, 1, 0, 0, 0, 0);
      const endMon = new Date (day.year, day.month, 1, 0, 0, 0, 0);

      const endMonth = endMon.getTime () - 1;

      return {nextStart: startMonth.getTime (), nextEnd: endMonth};

    default:
      const delta = 1 + end - start;
      const nextStart = start - delta;
      const nextEnd = end - delta;

      return {nextStart: nextStart, nextEnd: nextEnd};
  }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

export function moveNextTime (type, start, end) {
  switch (type) {
    case 'month':
      const day = dayNow (new Date (start));

      const startMonth = new Date (day.year, day.month + 1, 1, 0, 0, 0, 0);
      const endMon = new Date (day.year, day.month + 2, 1, 0, 0, 0, 0);

      const endMonth = endMon.getTime () - 1;

      return {nextStart: startMonth.getTime (), nextEnd: endMonth};

    default:
      const delta = 1 + end - start;
      const nextStart = start + delta;
      const nextEnd = end + delta;

      return {nextStart: nextStart, nextEnd: nextEnd};
  }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

export function weekNow () {
  const dateNow = dayNow ();

  const startWeek = dateNow.startDay - dateNow.day.getDay () * 86400000;
  const endWeek = dateNow.startDay + (7 - dateNow.day.getDay ()) * 86400000 - 1;

  return {startWeek: startWeek, endWeek: endWeek};
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

export function nowMonth () {
  const dateNow = dayNow ();

  const startMonth = new Date (dateNow.year, dateNow.month, 1, 0, 0, 0, 0);
  const nextMonth = new Date (dateNow.year, dateNow.month + 1, 1, 0, 0, 0, 0);

  const endMonth = nextMonth.getTime () - 86400001;

  return {startMonth: startMonth.getTime (), endMonth: endMonth};
}
