import React, {Component} from 'react';

// @material-ui/core components
import withStyles from '@material-ui/core/styles/withStyles';
import Grid from '@material-ui/core/Grid';
// core components
import GridItem from 'components/Grid/GridItem.jsx';
import Table from 'components/Table/Table.jsx';
import Card from 'components/Card/Card.jsx';
import CardHeader from 'components/Card/CardHeader.jsx';
import CardBody from 'components/Card/CardBody.jsx';
import CustomInput from 'components/CustomInput/CustomInput.jsx';
import Button from 'components/CustomButtons/Button.jsx';
import Aytocomplete from '../../components/Aytocomplete';
import Input from '@material-ui/core/Input';

import NativeSelect from '@material-ui/core/NativeSelect';

import FormControl from '@material-ui/core/FormControl';
///src/actions
import {AddExpenses} from '../../actions';
import {load} from '../../actions/userAction';
////////////
import {connect} from 'react-redux';

const styles = {
  cardCategoryWhite: {
    '&,& a,& a:hover,& a:focus': {
      color: 'rgba(255,255,255,.62)',
      margin: '0',
      fontSize: '14px',
      marginTop: '0',
      marginBottom: '0',
    },
    '& a,& a:hover,& a:focus': {
      color: '#FFFFFF',
    },
  },
  cardTitleWhite: {
    color: '#FFFFFF',
    marginTop: '0px',
    minHeight: 'auto',
    fontWeight: '300',
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: '3px',
    textDecoration: 'none',
    '& small': {
      color: '#777',
      fontSize: '65%',
      fontWeight: '400',
      lineHeight: '1',
    },
  },
};

const options = {
  year: 'numeric',
  month: 'long',
  day: 'numeric',
  weekday: 'long',
  timezone: 'UTC',
};

class Dashboard extends Component {
  constructor (props) {
    super (props);

    this.description = '';
    this.state = {
      category: '',
      value: '',
    };
  }

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////

  handleAddExpenses = () => {
    const {AddExpenses, orderCategory} = this.props;

    let category;
    if (this.state.category) {
      category = this.state.category;
    } else if (orderCategory) {
      category = orderCategory[0];
    }

    if (category || this.description || this.state.value)
      AddExpenses (category, this.description, this.state.value);
  };

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////

  handleChange = event => {
    this.setState ({category: event.target.value});
  };

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////

  handleChangeDescription = value => {
    this.description = value;
  };

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////

  handleChangeValue = event => {
    this.setState ({value: event.target.value});
  };

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////

  getBodySelect = () => {
    const {category, orderCategory} = this.props;

    let bodyCategory = [];

    const buildBody = (idCategory, parentIndex) => {
      bodyCategory.push (
        <option value={idCategory} key={idCategory}>
          {' '}{parentIndex + ' ' + category[idCategory].name}{' '}
        </option>
      );

      let parentChuild = 1;
      if (category[idCategory].child) {
        category[idCategory].child.forEach (idCategory => {
          let index = parentIndex + '.' + parentChuild;
          buildBody (idCategory, index);
          parentChuild = parentChuild + 1;
        });
      }
    };

    let parentIndex = 1;
    if (!category) {
      bodyCategory.push (
        <option value={this.state.category} key={0}> None</option>
      );
    } else {
      orderCategory.forEach (idCategory => {
        buildBody (idCategory, parentIndex);
        parentIndex = parentIndex + 1;
      });
    }
    return bodyCategory;
  };

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////

  getBodyTable = () => {
    const {expenses, category} = this.props;

    const table = Object.keys (expenses).sort ((a, b) => {
      return expenses[b].date - expenses[a].date;
    });
    const bodyTable = table.map (item => {
      let line = [];
      let name = ' ';

      if (category[expenses[item].category]) {
        if (category[expenses[item].category].name) {
          name = category[expenses[item].category].name;
        }
      }
      line.push (
        new Date (expenses[item].date).toLocaleString ('en-US', options)
      );
      line.push (name);
      line.push (expenses[item].description);
      line.push (expenses[item].value);

      return line;
    });

    return (
      <Table
        tableHeaderColor="primary"
        tableHead={['Date', 'Category', 'Expenses', 'Value UAH']}
        tableData={bodyTable}
      />
    );
  };

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////

  getSuggestions = () => {
    const {expenses} = this.props;

    let suggestions = [];
    let description = [];
    for (let item in expenses) {
      if (expenses[item].description) {
        if (description.indexOf (expenses[item].description) === -1) {
          description.push (expenses[item].description);
        }
      }
    }

    description.forEach (item => {
      suggestions.push ({label: item});
    });

    return suggestions;
  };

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////

  componentDidMount () {
    const {load} = this.props;
    load ();
  }

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////

  render () {
    const {classes} = this.props;
    return (
      <Grid container>
        <GridItem xs={12} sm={12} md={12}>
          <Card>
            <CardHeader color="primary" style={{background: '#26c6da'}}>
              <h4 className={classes.cardTitleWhite}>New expenses</h4>
              <p className={classes.cardCategoryWhite}>
                Please enter new expenses data here
              </p>
            </CardHeader>
            <CardBody>
              <Grid container alignItems="flex-end">

                <GridItem>
                  <FormControl className={classes.formControl}>
                    <NativeSelect
                      onChange={this.handleChange}
                      value={this.state.category}
                      style={{minWidth: '150px', marginBottom: '10px'}}
                      input={<Input name="name" id="uncontrolled-native" />}
                    >

                      {this.getBodySelect ()}

                    </NativeSelect>
                  </FormControl>
                </GridItem>

                <GridItem xs={3} sm={3} md={3}>
                  <Aytocomplete
                    onChange={this.handleChangeDescription}
                    suggestions={this.getSuggestions ()}
                  />
                </GridItem>

                <GridItem xs={2} sm={2} md={2}>
                  <CustomInput
                    labelText="UAH"
                    id="value"
                    inputProps={{
                      value: this.state.value,
                      onChange: this.handleChangeValue,
                    }}
                    formControlProps={{
                      fullWidth: true,
                    }}
                  />
                </GridItem>

                <GridItem>
                  <Button
                    color="primary"
                    onClick={this.handleAddExpenses}
                    style={{margin: '10px'}}
                  >
                    ADD EXPENSES
                  </Button>
                </GridItem>

              </Grid>

            </CardBody>
          </Card>
        </GridItem>
        <GridItem xs={12} sm={12} md={12}>
          <Card>
            <CardHeader color="primary" style={{background: '#26c6da'}}>
              <h4 className={classes.cardTitleWhite}>
                Latest expenses
              </h4>
              <p className={classes.cardCategoryWhite}>
                Here is latest 20 expenses
              </p>

            </CardHeader>
            <CardBody>

              {this.getBodyTable ()}

            </CardBody>
          </Card>
        </GridItem>
      </Grid>
    );
  }
}

const mapStateToProps = state => {
  return {
    category: state.content.category,
    orderCategory: state.content.filterContent.orderCategory,
    expenses: state.expenses,
  };
};

export default withStyles (styles) (
  connect (mapStateToProps, {AddExpenses, load}) (Dashboard)
);
