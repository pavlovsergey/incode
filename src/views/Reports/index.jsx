import React, {Component} from 'react';
// react plugin for creating charts
import ChartistGraph from 'react-chartist';
// @material-ui/core components
import withStyles from '@material-ui/core/styles/withStyles';
import Grid from '@material-ui/core/Grid';
// core components
import GridItem from 'components/Grid/GridItem.jsx';
import Table from 'components/Table/Table.jsx';
import Card from 'components/Card/Card.jsx';
import CardHeader from 'components/Card/CardHeader.jsx';
import CardBody from 'components/Card/CardBody.jsx';
import Button from 'components/CustomButtons/Button.jsx';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

import DateFnsUtils from 'material-ui-pickers/utils/date-fns-utils';
import MuiPickersUtilsProvider
  from 'material-ui-pickers/utils/MuiPickersUtilsProvider';
import DatePicker from 'material-ui-pickers/DatePicker';
//icon
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';

import {filterExpenses} from '../../actions';
import {connect} from 'react-redux';

import {
  dayNow,
  weekNow,
  nowMonth,
  moveBackTime,
  moveNextTime,
} from '../../utils/time';
import {emailsSubscriptionChart} from './variables';

const styles = {
  cardCategoryWhite: {
    '&,& a,& a:hover,& a:focus': {
      color: 'rgba(255,255,255,.62)',
      margin: '0',
      fontSize: '14px',
      marginTop: '0',
      marginBottom: '0',
    },
    '& a,& a:hover,& a:focus': {
      color: '#FFFFFF',
    },
  },
  cardTitleWhite: {
    color: '#FFFFFF',
    marginTop: '0px',
    minHeight: 'auto',
    fontWeight: '300',
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: '3px',
    textDecoration: 'none',
    '& small': {
      color: '#777',
      fontSize: '65%',
      fontWeight: '400',
      lineHeight: '1',
    },
  },
};

class Reports extends Component {
  constructor (props) {
    super (props);

    this.startDate = dayNow ().startDay;
    this.endDate = dayNow ().endDay;

    this.state = {
      openСalendar: false,
      selectedDateStart: this.startDate,
      selectedDateEnd: this.endDate,
      graphOpen: false,
      month: false,
    };
  }

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////

  handleOpenСalendar = () => {
    this.setState ({openСalendar: true});
    this.endDate = this.state.selectedDateEnd;
    this.startDate = this.state.selectedDateStart;
  };

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////

  handleCloseСalendar = () => {
    this.setState ({
      openСalendar: false,
      selectedDateEnd: this.endDate,
      selectedDateStart: this.startDate,
    });
  };

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////

  handleDateChangeStart = date => {
    this.setState ({selectedDateStart: date.getTime ()});
  };

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////

  handleDateChangeEnd = date => {
    this.setState ({selectedDateEnd: date.getTime () + 86400000 - 1});
  };

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////

  handlApplyDate = () => {
    const {filterExpenses} = this.props;

    const startDate = this.state.selectedDateStart;
    const endDate = this.state.selectedDateEnd;
    filterExpenses (startDate, endDate);

    this.setState ({openСalendar: false, month: false});
  };

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////

  handlApplyDay = () => {
    const {filterExpenses} = this.props;

    const day = dayNow ();

    this.setState ({
      openСalendar: false,
      selectedDateEnd: day.endDay,
      selectedDateStart: day.startDay,
      month: false,
    });
    filterExpenses (day.startDay, day.endDay);
  };

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////

  handlApplyMonth = () => {
    const {filterExpenses} = this.props;

    const monthNow = nowMonth ();

    this.setState ({
      openСalendar: false,
      selectedDateStart: monthNow.startMonth,
      selectedDateEnd: monthNow.endMonth,
      month: true,
    });
    filterExpenses (monthNow.startMonth, monthNow.endMonth);
  };

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////

  handlApplyWeek = () => {
    const {filterExpenses} = this.props;

    const week = weekNow ();

    this.setState ({
      openСalendar: false,
      selectedDateEnd: week.endWeek,
      selectedDateStart: week.startWeek,
      month: false,
    });
    filterExpenses (week.startWeek, week.endWeek);
  };

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////

  handlLeft = () => {
    const {filterExpenses} = this.props;
    const month = this.state.month ? 'month' : '';
    const start = this.state.selectedDateStart;
    const end = this.state.selectedDateEnd;

    const back = moveBackTime (month, start, end);

    this.setState ({
      selectedDateStart: back.nextStart,
      selectedDateEnd: back.nextEnd,
    });
    filterExpenses (back.nextStart, back.nextEnd);
  };

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////

  handlRight = () => {
    const {filterExpenses} = this.props;
    const month = this.state.month ? 'month' : '';
    const start = this.state.selectedDateStart;
    const end = this.state.selectedDateEnd;

    const next = moveNextTime (month, start, end);

    this.setState ({
      selectedDateStart: next.nextStart,
      selectedDateEnd: next.nextEnd,
    });
    filterExpenses (next.nextStart, next.nextEnd);
  };

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////

  handleOpenGraph = () => {
    this.setState ({graphOpen: this.state.graphOpen ? false : true});
  };

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////

  getBody = () => {
    const {category, orderCategory, loadExpenses} = this.props;

    // ##############################
    // // // ***preparation date***
    // #############################
    let bodyCategory = [];

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////

    const buildBody = (idCategory, parentIndex, parentNameIndex) => {
      bodyCategory.push ([
        parentNameIndex + ' ' + category[idCategory].name,
        category[idCategory].expenses,
      ]);
      let newParentIndex;
      const index = bodyCategory.length - 1;

      let parentChuild = 1;
      if (category[idCategory].child) {
        category[idCategory].child.forEach (idCategory => {
          let indexName = parentNameIndex + '.' + parentChuild;
          // eslint-disable-next-line
          if (parentIndex.indexOf (index) == -1) {
            newParentIndex = parentIndex.concat (index);
          }

          buildBody (idCategory, newParentIndex, indexName);
          parentChuild = parentChuild + 1;
        });
      }

      if (parentIndex)
        parentIndex.forEach (parentIndex => {
          // eslint-disable-next-line
          if (parentIndex != index) {
            bodyCategory[parentIndex][1] =
              bodyCategory[parentIndex][1] + category[idCategory].expenses;
          }
        });
    };

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////

    let parentIndex = 1;
    if (!category || !loadExpenses) {
      bodyCategory.push ([]);
    } else {
      orderCategory.forEach (idCategory => {
        buildBody (idCategory, [], parentIndex);
        parentIndex = parentIndex + 1;
      });
    }
    // ##################################
    // // // ***preparation date END***
    // #################################

    // ##################################
    // // // ******create body******
    // #################################

    if (!this.state.graphOpen) {
      const stringBodyCategory = bodyCategory.map (item => {
        return [item[0], item[1] + ''];
      });

      return (
        <Table
          tableHeaderColor="primary"
          tableHead={['Category', 'Expenses value, UAH']}
          tableData={stringBodyCategory}
        />
      );
    } else {
      emailsSubscriptionChart.data.labels = [];
      emailsSubscriptionChart.data.series[0] = [];

      let maxValue;

      bodyCategory.forEach (item => {
        emailsSubscriptionChart.data.labels.push (item[0]);
        emailsSubscriptionChart.data.series[0].push (item[1]);
        if (item[1] > maxValue) maxValue = item[1];
      });
      emailsSubscriptionChart.options.high = maxValue;
      return (
        <div style={{background: '#002121', color: '#212121'}}>
          <ChartistGraph
            className="ct-chart"
            data={emailsSubscriptionChart.data}
            type="Bar"
            options={emailsSubscriptionChart.options}
            responsiveOptions={emailsSubscriptionChart.responsiveOptions}
            listener={emailsSubscriptionChart.animation}
          />
          {' '}
        </div>
      );
    }
  };

  // ##################################
  // // // ******create body END******
  // #################################
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////

  componentDidMount () {
    const {filterExpenses} = this.props;
    const day = dayNow ();

    filterExpenses (day.startDay, day.endDay);
  }

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////

  render () {
    const {classes} = this.props;
    const {selectedDateEnd, selectedDateStart} = this.state;
    let dateStart = new Date (selectedDateStart);
    let dateEnd = new Date (selectedDateEnd);

    const options = {
      year: 'numeric',
      month: 'long',
      day: 'numeric',
      weekday: 'long',
      timezone: 'UTC',
    };

    return (
      <Grid container>
        <Dialog
          open={this.state.openСalendar}
          onClose={this.handleCloseСalendar}
          aria-labelledby="form-dialog-title"
          fullWidth={true}
          maxWidth={false}
        >
          <DialogTitle id="form-dialog-title">Select period</DialogTitle>
          <DialogContent>

            <MuiPickersUtilsProvider utils={DateFnsUtils}>
              <DatePicker
                format="DD/MM/YYYY"
                value={selectedDateStart}
                onChange={this.handleDateChangeStart}
              />

              <DatePicker
                format="DD/MM/YYYY"
                value={selectedDateEnd}
                onChange={this.handleDateChangeEnd}
              />
            </MuiPickersUtilsProvider>

          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleCloseСalendar} color="primary">
              Exit
            </Button>
            <Button onClick={this.handlApplyDate} color="primary">
              Apply
            </Button>
          </DialogActions>
        </Dialog>
        <GridItem xs={12} sm={12} md={12}>
          <Card>
            <CardHeader color="primary" style={{background: '#26c6da'}}>
              <h4 className={classes.cardTitleWhite}>Expenses reports</h4>
              <p className={classes.cardCategoryWhite}>
                Here is some expenses reports
              </p>
            </CardHeader>
            <CardBody>
              <h5
                style={{
                  color: '#BDBDBD',
                  fontWeight: '900',
                  display: 'inline',
                  marginRight: '30px',
                }}
              >
                {dateStart.toLocaleString ('en-US', options)}
                {' '}
                /
                {' '}
                {dateEnd.toLocaleString ('en-US', options)}
              </h5>
              <Button
                color="primary"
                style={{marginRight: '30px'}}
                onClick={this.handlLeft}
              >
                {' '} <KeyboardArrowLeft />{' '}
              </Button>
              <Button
                color="primary"
                style={{marginRight: '30px'}}
                onClick={this.handlRight}
              >
                {' '}<KeyboardArrowRight />{' '}
              </Button>
              <Button
                color="primary"
                style={{marginRight: '30px'}}
                onClick={this.handlApplyDay}
              >
                Day
              </Button>
              <Button
                color="primary"
                style={{marginRight: '30px'}}
                onClick={this.handlApplyWeek}
              >
                Week
              </Button>
              <Button
                color="primary"
                style={{marginRight: '30px'}}
                onClick={this.handlApplyMonth}
              >
                Month
              </Button>
              <Button
                color="primary"
                onClick={this.handleOpenСalendar}
                style={{marginRight: '30px'}}
              >
                Period
              </Button>
              <Button
                color="primary"
                onClick={this.handleOpenGraph}
                style={{marginRight: '30px'}}
              >
                Graph
              </Button>

              {this.getBody ()}

            </CardBody>
          </Card>
        </GridItem>
      </Grid>
    );
  }
}

const mapStateToProps = state => {
  return {
    category: state.content.category,
    orderCategory: state.content.filterContent.orderCategory,
    loadExpenses: state.loadExpenses,
  };
};

export default withStyles (styles) (
  connect (mapStateToProps, {filterExpenses}) (Reports)
);
