import React from 'react';
import {shallow} from 'enzyme';
import {shallowToJson} from 'enzyme-to-json';
import {Signin} from '../SignIn';
import {SignUp} from '../SignUp';
import {CustomInput} from '../../components/CustomInput/CustomInput';

describe ('__snapshots__ views', () => {
  it ('should render Signin', () => {
    const props = {
      classes: {
        cardCategoryWhite: 'ReduxForm-cardCategoryWhite-172',
        cardTitleWhite: 'ReduxForm-cardTitleWhite-173',
      },
      handleSubmit: jest.fn (),
      submitting: false,
      error: {},
    };

    const output = shallow (<Signin {...props} />);
    expect (shallowToJson (output)).toMatchSnapshot ();
  });

  it ('should render SignUp', () => {
    const props = {
      classes: {
        cardCategoryWhite: 'ReduxForm-cardCategoryWhite-172',
        cardTitleWhite: 'ReduxForm-cardTitleWhite-173',
      },
      handleSubmit: jest.fn (),
      submitting: false,
      error: {},
      message: '',
    };

    const output = shallow (<SignUp {...props} />);
    expect (shallowToJson (output)).toMatchSnapshot ();
  });
});

describe ('__snapshots__ components', () => {
  it ('should render CustomInput', () => {
    const props = {
      classes: {
        cardCategoryWhite: 'ReduxForm-cardCategoryWhite-172',
        cardTitleWhite: 'ReduxForm-cardTitleWhite-173',
      },
      labelText: '',
      labelProps: {},
      id: 'PropTypes.string',
      inputProps: {},
      formControlProps: {},
      error: false,
      success: false,
      meta: {},
      input: {},
    };

    const output = shallow (<CustomInput {...props} />);
    expect (shallowToJson (output)).toMatchSnapshot ();
  });
});
