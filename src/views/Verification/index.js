import React, {Component} from 'react';
// @material-ui/core components
import withStyles from '@material-ui/core/styles/withStyles';
import Grid from '@material-ui/core/Grid';
// core components
import GridItem from 'components/Grid/GridItem.jsx';
import CustomInput from 'components/CustomInput/CustomInput.jsx';
import Button from 'components/CustomButtons/Button.jsx';
import Card from 'components/Card/Card.jsx';
import CardHeader from 'components/Card/CardHeader.jsx';
import CardBody from 'components/Card/CardBody.jsx';
import CardFooter from 'components/Card/CardFooter.jsx';
import {NavLink} from 'react-router-dom';

import {connect} from 'react-redux';
import {verification} from '../../ducks/auth';

const styles = {
  cardCategoryWhite: {
    color: 'rgba(255,255,255,.62)',
    margin: '0',
    fontSize: '14px',
    marginTop: '0',
    marginBottom: '0',
  },
  cardTitleWhite: {
    color: '#FFFFFF',
    marginTop: '0px',
    minHeight: 'auto',
    fontWeight: '300',
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: '3px',
    textDecoration: 'none',
  },
};

class Verification extends Component {
  state = {
    email: new URL (window.location.href).searchParams.get ('name'),
    code: new URL (window.location.href).searchParams.get ('code'),
  };

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////

  handleVerification = () => {
    const {verification} = this.props;
    const {email, code} = this.state;

    verification (email, code);
  };

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////

  render () {
    const {classes} = this.props;
    return (
      <div>
        <Grid container>
          <GridItem xs={12} sm={12} md={8}>
            <Card>
              <CardHeader color="primary">
                <h4 className={classes.cardTitleWhite}>
                  Sign into Home Expense App
                </h4>
                <p className={classes.cardCategoryWhite}>
                  Pleas, entry your email and password{' '}
                </p>
              </CardHeader>
              <CardBody>
                <Grid container>

                  <GridItem xs={12} sm={12} md={6}>
                    <CustomInput
                      labelText="Email address"
                      id="email-address"
                      formControlProps={{
                        fullWidth: true,
                      }}
                      inputProps={{
                        disabled: true,
                        value: this.state.email,
                      }}
                    />
                  </GridItem>

                </Grid>
                <Grid container>

                  <GridItem xs={12} sm={12} md={6}>
                    <CustomInput
                      labelText="VerificationCode"
                      id="verificationCode"
                      formControlProps={{
                        fullWidth: true,
                      }}
                      inputProps={{
                        disabled: true,
                        value: this.state.code,
                      }}
                    />
                  </GridItem>

                </Grid>
              </CardBody>
              <CardFooter direction="column">
                <Grid container direction="column">

                  <GridItem xs={5} sm={5} md={5}>
                    <Button color="primary" onClick={this.handleVerification}>
                      Verifi Email
                    </Button>
                    <NavLink style={{display: 'block'}} to="/signin">
                      already have an account?sign-in
                    </NavLink>
                  </GridItem>

                </Grid>
              </CardFooter>
            </Card>
          </GridItem>
        </Grid>
      </div>
    );
  }
}

export default withStyles (styles) (
  connect (null, {verification}) (Verification)
);
