import React, {Component} from 'react';
// @material-ui/core components
import withStyles from '@material-ui/core/styles/withStyles';
import Grid from '@material-ui/core/Grid';
// core components
import GridItem from 'components/Grid/GridItem.jsx';
import CustomInput from 'components/CustomInput/CustomInput.jsx';
import Button from 'components/CustomButtons/Button.jsx';
import Card from 'components/Card/Card.jsx';
import CardHeader from 'components/Card/CardHeader.jsx';
import CardBody from 'components/Card/CardBody.jsx';
import CardFooter from 'components/Card/CardFooter.jsx';
import {NavLink} from 'react-router-dom';
import {connect} from 'react-redux';

import {signUp} from '../../ducks/auth';
import {reduxForm, Field} from 'redux-form';
import emailValidator from 'email-validator';
import passwordValidator from 'password-validator';

const styles = {
  cardCategoryWhite: {
    color: 'rgba(255,255,255,.62)',
    margin: '0',
    fontSize: '14px',
    marginTop: '0',
    marginBottom: '0',
  },
  cardTitleWhite: {
    color: '#FFFFFF',
    marginTop: '0px',
    minHeight: 'auto',
    fontWeight: '300',
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: '3px',
    textDecoration: 'none',
  },
};

export class SignUp extends Component {
  onEnterPress = e => {
    if (e.keyCode == 13 && e.shiftKey == false) {
      e.preventDefault ();
      const event = new Event ('submit');
      this.myFormRef.dispatchEvent (event);
    }
  };

  render () {
    const {classes, handleSubmit, submitting, error, message} = this.props;
    const mess = message
      ? <a href={message}> Click link for verification</a>
      : '';
    const errorText = (error && error.message) || mess || '';
    return (
      <div>
        <Grid container>
          <GridItem xs={12} sm={12} md={8}>
            <Card>

              <form
                onSubmit={handleSubmit}
                ref={el => (this.myFormRef = el)}
                onKeyDown={this.onEnterPress}
              >

                <CardHeader color="primary">
                  <h4 className={classes.cardTitleWhite}>
                    Registred with Home Expense App
                  </h4>
                  <p className={classes.cardCategoryWhite}>
                    Pleas, entry your email and password
                  </p>

                </CardHeader>
                <CardBody>
                  <p style={{fontSize: '18px', color: '#BF360C'}}>
                    {errorText}
                  </p>
                  <Grid container>

                    <GridItem xs={12} sm={12} md={6}>
                      <Field
                        name="email"
                        component={CustomInput}
                        labelText="Email address"
                        id="email-address"
                        formControlProps={{
                          fullWidth: true,
                        }}
                      />
                    </GridItem>

                  </Grid>
                  <Grid container>

                    <GridItem xs={12} sm={12} md={6}>
                      <Field
                        name="password"
                        component={CustomInput}
                        labelText="Password"
                        id="password"
                        formControlProps={{
                          fullWidth: true,
                        }}
                        inputProps={{
                          type: 'password',
                        }}
                      />
                    </GridItem>

                  </Grid>
                  <Grid container>

                    <GridItem xs={12} sm={12} md={6}>
                      <Field
                        name="repeatPassword"
                        component={CustomInput}
                        labelText="Repeat password"
                        id="Repeat password"
                        formControlProps={{
                          fullWidth: true,
                        }}
                        inputProps={{
                          type: 'password',
                        }}
                      />
                    </GridItem>

                  </Grid>
                </CardBody>
                <CardFooter direction="column">
                  <Grid container direction="column">
                    <GridItem xs={5} sm={5} md={5}>
                      <Button
                        color="primary"
                        onClick={handleSubmit}
                        disabled={submitting}
                      >
                        SIGN UP
                      </Button>
                      <NavLink style={{display: 'block'}} to="/signin">
                        alredy have an account? sig-in
                      </NavLink>
                    </GridItem>
                  </Grid>
                </CardFooter>
              </form>
            </Card>
          </GridItem>
        </Grid>
      </div>
    );
  }
}

// Create a schema
let schemaPasswordValidator = new passwordValidator ();

// Add properties to it
schemaPasswordValidator
  .is ()
  .min (8) // Minimum length 8
  .is ()
  .max (100) // Maximum length 100
  .has ()
  .uppercase () // Must have uppercase letters
  .has ()
  .lowercase () // Must have lowercase letters
  .has ()
  .digits () // Must have digits
  .is ()
  .not ()
  .oneOf (['Password', 'Passw0rd', 'Password123', 'password']); // Blacklist these values

const validate = ({email, password, repeatPassword}) => {
  const errors = {};
  if (!email) errors.email = 'email is required';
  else if (!emailValidator.validate (email)) {
    errors.email = 'invalid email';
  }

  const keyPasswordError = {
    min: ' minimum length 8;',
    max: ' maximum length 100;',
    uppercase: ' uppercase letters;',
    lowercase: ' lowercase letters;',
    digits: ' digits;',
    oneOf: ' be not primitive;',
  };
  if (!password) errors.password = 'password is required';
  else if (
    schemaPasswordValidator.validate (password, {list: true}).length > 0
  ) {
    errors.password = 'Password must have:';
    schemaPasswordValidator
      .validate (password, {list: true})
      .forEach (error => {
        errors.password += keyPasswordError[error] || '';
      });
  }
  if (password !== repeatPassword)
    errors.repeatPassword =
      'The password and confirm password fields do not match.';
  return errors;
};

const mapStateToProps = state => ({
  message: state.auth.message,
});

export default withStyles (styles) (
  reduxForm ({
    form: 'addUser', // имя формы в state (state.form.auth) // a unique name for the form
    validate,
    onSubmit: signUp,
  }) (connect (mapStateToProps) (SignUp))
);
