import React, {Component} from 'react';
// @material-ui/core components
import withStyles from '@material-ui/core/styles/withStyles';
import Grid from '@material-ui/core/Grid';
// core components
import GridItem from 'components/Grid/GridItem.jsx';

import TableRow from '@material-ui/core/TableRow';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import Card from 'components/Card/Card.jsx';
import CardHeader from 'components/Card/CardHeader.jsx';
import CardBody from 'components/Card/CardBody.jsx';
import Button from 'components/CustomButtons/Button.jsx';
import CardFooter from 'components/Card/CardFooter.jsx';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

import ConfigCategory from './ConfigCategory';
import Category from './Category';
import {AddCategory} from '../../actions';
import {connect} from 'react-redux';
import FreeCategory from './FreeCategory';

const styles = {
  cardCategoryWhite: {
    '&,& a,& a:hover,& a:focus': {
      color: 'rgba(255,255,255,.62)',
      margin: '0',
      fontSize: '14px',
      marginTop: '0',
      marginBottom: '0',
    },
    '& a,& a:hover,& a:focus': {
      color: '#FFFFFF',
    },
  },
  cardTitleWhite: {
    color: '#FFFFFF',
    marginTop: '0px',
    minHeight: 'auto',
    fontWeight: '300',
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: '3px',
    textDecoration: 'none',
    '& small': {
      color: '#777',
      fontSize: '65%',
      fontWeight: '400',
      lineHeight: '1',
    },
  },
};

class Config extends Component {
  constructor (props) {
    super (props);

    this.state = {
      openConfig: false,
      currentId: '',
      openFree: false,
    };

    this.index = '';
  }

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////

  handleOpenConfig = (currentId, index) => {
    this.index = index;
    this.setState ({openConfig: true, currentId: currentId});
  };

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////

  handleClose = () => {
    this.setState ({openConfig: false});
  };

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////

  handleAdd = () => {
    this.props.AddCategory ();
  };

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////

  handleAddFree = () => {
    this.setState ({openFree: true});
  };

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////

  handleCloseFree = () => {
    this.setState ({openFree: false});
  };

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////

  render () {
    const {classes, category, orderCategory} = this.props;

    let bodyCategory = [];
    const buildBody = (idCategory, parentIndex) => {
      if (this.state.currentId == idCategory) this.index = parentIndex;
      bodyCategory.push (
        <Category
          id={idCategory}
          index={parentIndex}
          name={' ' + category[idCategory].name}
          parentId={category[idCategory].parentCat}
          handleOpenConfig={this.handleOpenConfig}
          key={Date.now () + parentIndex}
          config={'false'}
        />
      );

      let parentChuild = 1;
      if (category[idCategory].child) {
        category[idCategory].child.forEach (idCategory => {
          let index = parentIndex + '.' + parentChuild;
          buildBody (idCategory, index);
          parentChuild = parentChuild + 1;
        });
      }
    };

    if (!category) {
      bodyCategory.push (<TableRow key={0} />);
    } else {
      let parentIndex = 1;
      orderCategory.forEach (idCategory => {
        buildBody (idCategory, parentIndex);
        parentIndex = parentIndex + 1;
      });
    }

    return (
      <Grid container>

        <Dialog
          open={this.state.openConfig}
          onClose={this.handleClose}
          aria-labelledby="form-dialog-title"
          fullWidth={true}
          maxWidth={false}
        >
          <DialogTitle id="form-dialog-title">Сhildren</DialogTitle>
          <DialogContent>

            <ConfigCategory id={this.state.currentId} />

          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              Exit
            </Button>
            <Button onClick={this.handleAddFree} color="primary">
              ADD CATEGORI
            </Button>
          </DialogActions>
        </Dialog>

        <Dialog
          open={this.state.openFree}
          onClose={this.handleCloseFree}
          aria-labelledby="form-dialog-title"
          fullWidth={true}
          maxWidth={false}
        >
          <DialogTitle id="form-dialog-title">Сhildren</DialogTitle>
          <DialogContent>

            <FreeCategory
              id={this.state.currentId}
              parentId={this.state.currentId}
              index={this.index}
            />

          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleCloseFree} color="primary">
              Exit
            </Button>

          </DialogActions>
        </Dialog>

        <GridItem xs={12} sm={12} md={12}>
          <Card>
            <CardHeader color="primary">
              <h4 className={classes.cardTitleWhite}>Edit Categories</h4>
              <p className={classes.cardCategoryWhite}>
                Please, config your categories
              </p>
            </CardHeader>
            <CardBody>
              <Table className={classes.table}>

                <TableBody>
                  {bodyCategory}
                </TableBody>

              </Table>
            </CardBody>
            <CardFooter direction="column">
              <Grid container direction="column">

                <GridItem xs={3} sm={3} md={3}>
                  <Button color="primary" onClick={this.handleAdd}>
                    ADD CATEGORI
                  </Button>
                </GridItem>

              </Grid>
            </CardFooter>
          </Card>
        </GridItem>
      </Grid>
    );
  }
}
const mapStateToProps = state => {
  return {
    category: state.content.category,
    orderCategory: state.content.filterContent.orderCategory,
  };
};

export default withStyles (styles) (
  connect (mapStateToProps, {AddCategory}) (Config)
);
