import React, {Component} from 'react';

import Category from './Category';
import TableRow from '@material-ui/core/TableRow';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';

import {AddCategory} from '../../actions';
import {connect} from 'react-redux';

class FreeCategory extends Component {
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////

  handleAdd = () => {
    this.props.AddCategory ();
  };

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////

  render () {
    const {id, category, orderCategory, parentId, index} = this.props;

    let bodyCategory = [];
    if (!orderCategory) {
      bodyCategory.push (<TableRow key={0} />);
    } else {
      let nameIndex = 1;
      const headParent = index.toString ().charAt (0)
        ? index.toString ().charAt (0)
        : true;

      orderCategory.forEach (idCategory => {
        // eslint-disable-next-line
        if (headParent != nameIndex) {
          if (idCategory !== id) {
            bodyCategory.push (
              <Category
                id={idCategory}
                name={nameIndex + ' ' + category[idCategory].name}
                parentId={parentId}
                handleOpenConfig={this.handleOpenConfig}
                config={'free category'}
                key={idCategory}
              />
            );
          }
        }
        nameIndex = nameIndex + 1;
      });
    }

    return (
      <Table>

        <TableBody>
          {bodyCategory}
        </TableBody>

      </Table>
    );
  }
}

const mapStateToProps = state => {
  return {
    category: state.content.category,
    orderCategory: state.content.filterContent.orderCategory,
  };
};

export default connect (mapStateToProps, {AddCategory}) (FreeCategory);
