import React, {Component} from 'react';

import Category from './Category';
import TableRow from '@material-ui/core/TableRow';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';

import {AddCategory} from '../../actions';
import {connect} from 'react-redux';

class ConfigCategory extends Component {
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////

  handleAdd = () => {
    this.props.AddCategory ();
  };

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////

  render () {
    const {id, category} = this.props;

    let bodyCategory = [];
    const buildBody = idCategory => {
      category[idCategory].child.forEach (idCategory => {
        bodyCategory.push (
          <Category
            id={idCategory}
            name={category[idCategory].name}
            parentId={category[idCategory].parentCat}
            handleOpenConfig={this.handleOpenConfig}
            config={'config category'}
            key={idCategory}
          />
        );
      });
    };

    if (!category[id].child) {
      bodyCategory.push (<TableRow key={0} />);
    } else {
      buildBody (id);
    }

    return (
      <Table>

        <TableBody>
          {bodyCategory}
        </TableBody>

      </Table>
    );
  }
}

const mapStateToProps = state => {
  return {
    category: state.content.category,
    orderCategory: state.content.filterContent.orderCategory,
  };
};

export default connect (mapStateToProps, {AddCategory}) (ConfigCategory);
