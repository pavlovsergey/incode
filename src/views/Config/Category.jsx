import React, {Component} from 'react';
//icon
import ArrowUpward from '@material-ui/icons/ArrowUpward';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import Cancel from '@material-ui/icons/Cancel';
// core components
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import Button from 'components/CustomButtons/Button.jsx';
import Input from '@material-ui/core/Input';
// actions
import {
  deleteCategory,
  ChangeOrderCategoryUp,
  ChangeOrderCategoryDown,
  saveNameCategory,
  AddParent,
  deleteParent,
} from '../../actions';
// redux
import {connect} from 'react-redux';

class Category extends Component {
  state = {
    value: this.props.name,
    saveName: false,
  };

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  handleDelete = () => {
    const {id, deleteCategory, parentId, config, deleteParent} = this.props;
    // eslint-disable-next-line
    if (config == 'config category') {
      deleteParent (id, parentId);
    } else {
      deleteCategory (id, parentId);
    }
  };

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  handleChange = e => {
    this.setState ({value: e.target.value, saveName: true});
  };

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  handleSaveName = () => {
    const newName = this.state.value;
    const id = this.props.id;
    this.props.saveNameCategory (newName, id);
    this.setState ({saveName: false});
  };

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  handleUP = () => {
    const {id, ChangeOrderCategoryUp, parentId} = this.props;
    ChangeOrderCategoryUp (id, parentId);
  };

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  handleDown = () => {
    const {id, ChangeOrderCategoryDown, parentId} = this.props;
    ChangeOrderCategoryDown (id, parentId);
  };

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  handleOpen = () => {
    const {id, handleOpenConfig, index} = this.props;
    handleOpenConfig (id, index);
  };

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  handleAddParent = () => {
    const {id, AddParent, parentId} = this.props;
    AddParent (id, parentId);
  };

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  render () {
    const {config, index, key} = this.props;
    const styleSaveName = this.state.saveName
      ? {background: '#EF5350'}
      : {visibility: 'hidden'};

    let styleConfig = {background: '#26c6da'};

    let disabled = false;
    // eslint-disable-next-line
    if (config == 'config category' || config == 'free category') {
      styleConfig = {visibility: 'hidden'};
      disabled = true;
    }

    let styleFreeCategory = {
      yellow: {background: '#FF9800'},
      blue: {background: '#26c6da'},
      add: {visibility: 'hidden'},
    };
    // eslint-disable-next-line
    if (config == 'free category') {
      styleFreeCategory = {
        yellow: {visibility: 'hidden'},
        blue: {visibility: 'hidden'},
        add: {background: '#26c6da'},
      };
      disabled = true;
    }

    return (
      <TableRow key={key}>

        <TableCell component="th" scope="row">

          <span> {index} </span>

          <Input
            value={this.state.value}
            onChange={this.handleChange}
            inputProps={{
              disabled: disabled,
            }}
          />
        </TableCell>

        <TableCell>
          <Button
            color="primary"
            style={styleFreeCategory.blue}
            onClick={this.handleUP}
          >
            <ArrowUpward />
          </Button>
          <Button
            color="primary"
            style={styleFreeCategory.blue}
            onClick={this.handleDown}
          >
            <ArrowDownward />
          </Button>
          <Button
            color="primary"
            onClick={this.handleDelete}
            style={styleFreeCategory.yellow}
          >
            <Cancel />
          </Button>
          <Button color="primary" style={styleConfig} onClick={this.handleOpen}>
            *
          </Button>
          <Button
            color="primary"
            style={styleSaveName}
            onClick={this.handleSaveName}
          >
            save name
          </Button>
          <Button
            color="primary"
            style={styleFreeCategory.add}
            onClick={this.handleAddParent}
          >
            Add
          </Button>
        </TableCell>

      </TableRow>
    );
  }
}

export default connect (null, {
  deleteCategory,
  saveNameCategory,
  ChangeOrderCategoryUp,
  ChangeOrderCategoryDown,
  AddParent,
  deleteParent,
}) (Category);
