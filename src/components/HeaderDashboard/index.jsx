import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
// @material-ui/core components
import withStyles from '@material-ui/core/styles/withStyles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Hidden from '@material-ui/core/Hidden';
// @material-ui/icons
import Menu from '@material-ui/icons/Menu';
// core components
import HeaderLinks from './HeaderLinks';

import headerStyle
  from 'assets/jss/material-dashboard-react/components/headerStyle.jsx';

function Header({...props}) {
  function makeBrand () {
    var name;
    props.routes.map ((prop, key) => {
      if (prop.path === props.location.pathname) {
        name = prop.navbarName;
      }
      return null;
    });
    return name;
  }
  const {classes, color} = props;
  const appBarClasses = classNames ({
    [' ' + classes[color]]: color,
  });
  return (
    <AppBar className={classes.appBar + appBarClasses}>
      <Toolbar className={classes.container}>
        <div className={classes.flex}>
          <div
            style={{
              margin: '0.3125rem 1px',
              fontSize: '18px',
              fontFamily: 'Roboto, Helvetica, Arial, sans-serif',
              fontWeight: 320,
              lineHeight: '30px',
              padding: '12px 30px',
            }}
          >
            {makeBrand ()}
          </div>
        </div>
        <Hidden smDown implementation="css">
          <HeaderLinks />
        </Hidden>
        <Hidden mdUp>
          <IconButton
            className={classes.appResponsive}
            color="inherit"
            aria-label="open drawer"
            onClick={props.handleDrawerToggle}
          >
            <Menu />
          </IconButton>
        </Hidden>
      </Toolbar>
    </AppBar>
  );
}

Header.propTypes = {
  classes: PropTypes.object.isRequired,
  color: PropTypes.oneOf (['primary', 'info', 'success', 'warning', 'danger']),
};

export default withStyles (headerStyle) (Header);
