// @material-ui/icons
import Person from '@material-ui/icons/Person';
// core components/views
import Signin from 'views/SignIn';
import SignUp from 'views/SignUp';
import Verification from 'views/Verification';

const dashboardRoutes = [
  {
    path: '/signin',
    sidebarName: 'Sign in',
    navbarName: 'Sign in',
    icon: Person,
    component: Signin,
  },
  {
    path: '/signup',
    sidebarName: 'Sign up',
    navbarName: 'Sign up',
    icon: Person,
    component: SignUp,
  },
  {
    path: '/verification',
    sidebarName: '',
    navbarName: 'Email verification',
    icon: Person,
    component: Verification,
  },
  {redirect: true, path: '/', to: '/signin', navbarName: 'Redirect'},
];

export default dashboardRoutes;
