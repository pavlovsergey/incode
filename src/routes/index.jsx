import Dashboard from 'layouts/Dashboard/Dashboard.jsx';
import SignIn from 'layouts/SignIn/SignIn.jsx';

function indexRoutes (loading) {
  if (window.localStorage.getItem ('jwt') && loading)
    return [{path: '/', component: Dashboard}];
  return [{path: '/', component: SignIn}];
}
export default indexRoutes;
