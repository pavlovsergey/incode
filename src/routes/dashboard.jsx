// @material-ui/icons
import Dashboard from '@material-ui/icons/Dashboard';
import Settings from '@material-ui/icons/Settings';
import EventNote from '@material-ui/icons/EventNote';

// core components/views
import DashboardPage from 'views/Dashboard/Dashboard.jsx';

import Config from 'views/Config';
import Reports from 'views/Reports';

const dashboardRoutes = [
  {
    path: '/dashboard',
    sidebarName: 'Dashboard',
    navbarName: 'Dashboard',
    icon: Dashboard,
    component: DashboardPage,
  },
  {
    path: '/reports',
    sidebarName: 'Reports',
    navbarName: 'Reports',
    icon: EventNote,
    component: Reports,
  },
  {
    path: '/config',
    sidebarName: 'Config',
    navbarName: 'Config',
    icon: Settings,
    component: Config,
  },

  {redirect: true, path: '/', to: '/dashboard', navbarName: 'Redirect'},
];

export default dashboardRoutes;
