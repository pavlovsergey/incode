import React from 'react';
import ReactDOM from 'react-dom';

import 'assets/css/material-dashboard-react.css?v=1.3.0';

import App from './App';

import {Provider} from 'react-redux';
import store from './store';

ReactDOM.render (
  <div>
    <Provider store={store}>
      <App />
    </Provider>
  </div>,
  document.getElementById ('root')
);
