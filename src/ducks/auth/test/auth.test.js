import {signIn, signUp, verification, unSignIn} from '../';
import {SIGN_IN_SUCCESS, SIGN_IN_OUT, SIGN_UP_MESSAGE} from '../';
import reducer from '../';
import * as axiosForTest from '../../../utils/axios';
import {Record} from 'immutable';

// Action Creators

axiosForTest.axs = jest.fn (() => Promise.resolve ({data: {token: 'token'}}));
const user = {email: 'email', password: 'password'};

const localStorageMock = {
  getItem: jest.fn (),
  setItem: jest.fn (),
  clear: jest.fn (),
};
global.localStorage = localStorageMock;

describe ('test Action Creators ducks auth', () => {
  describe ('function signIn resolve', () => {
    const fakeDispatch = jest.fn ();

    it ('Dispatch in signIn', () => {
      return signIn (user, fakeDispatch).then (() => {
        expect (fakeDispatch.mock.calls).toEqual ([[{type: SIGN_IN_SUCCESS}]]);
      });
    });

    it ('localStorage in signIn', () => {
      expect (global.localStorage.setItem.mock.calls).toEqual ([
        ['jwt', 'token'],
      ]);
    });
  });

  describe ('function signUp resolve', () => {
    const fakeDispatch = jest.fn ();

    it ('Dispatch in signUp', () => {
      return signUp (user, fakeDispatch).then (() => {
        expect (fakeDispatch.mock.calls).toEqual ([
          [
            {
              payload: {message: {token: 'token'}},
              type: SIGN_UP_MESSAGE,
            },
          ],
        ]);
      });
    });
  });

  describe ('function verification resolve', () => {
    const fakeDispatch = jest.fn ();

    it ('Dispatch in verification', () => {
      return verification (user) (fakeDispatch).then (() => {
        expect (fakeDispatch.mock.calls).toEqual ([[{type: SIGN_IN_SUCCESS}]]);
      });
    });

    it ('localStorage in verification', () => {
      expect (global.localStorage.setItem.mock.calls[1]).toEqual ([
        'jwt',
        'token',
      ]);
    });
  });

  it ('localStorage in unSignIn', () => {
    expect (unSignIn ()).toEqual ({type: SIGN_IN_OUT});
  });
});

// Reducer

describe ('reducer', () => {
  it ('should return the initial state', () => {
    expect (reducer (undefined, {}).toJSON ()).toEqual ({
      auth: false,
      message: '',
    });
  });

  it ('should handle SIGN_IN_SUCCESS', () => {
    expect (reducer (undefined, {type: SIGN_IN_SUCCESS}).toJSON ()).toEqual ({
      auth: true,
      message: '',
    });
  });

  it ('should  handle SIGN_IN_OUT', () => {
    const initialState = Record ({
      auth: true,
      message: '',
    });
    expect (reducer (new initialState (), {type: SIGN_IN_OUT})).toEqual (
      new initialState ().set ('auth', false)
    );
  });
});

it ('should return the initial state', () => {
  const initialState = Record ({
    auth: false,
    message: '',
  });
  expect (
    reducer (new initialState (), {
      type: SIGN_UP_MESSAGE,
      payload: {message: 'test'},
    }).toJSON ()
  ).toEqual ({
    auth: false,
    message: 'test',
  });
});
