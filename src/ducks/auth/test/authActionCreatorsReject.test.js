import {signIn, signUp, verification} from '../';
import * as axiosForTest from '../../../utils/axios';

axiosForTest.axs = jest.fn (() =>
  Promise.reject ({
    message: 'test',
    name: 'test',
  })
);

const localStorageMock = {
  getItem: jest.fn (),
  setItem: jest.fn (),
  removeItem: jest.fn (),
};
global.localStorage = localStorageMock;

const user = {email: 'email', password: 'password'};

const fakeDispatch = jest.fn ();

describe ('test Action Creators ducks auth (reject)', () => {
  it ('Error type message in signIn', () => {
    return signIn (user, fakeDispatch).catch (e => {
      expect (e.message).toEqual ('Submit Validation Failed');
      expect (e.errors._error.message).toEqual ('test');
      expect (e.errors._error.name).toEqual ('test');
    });
  });

  it ('Error type message in signUp', () => {
    return signUp (user, fakeDispatch).catch (e => {
      expect (e.message).toEqual ('Submit Validation Failed');
      expect (e.errors._error.message).toEqual ('test');
      expect (e.errors._error.name).toEqual ('test');
    });
  });

  it ('Error in verification', () => {
    return verification (user) (fakeDispatch).then (() => {
      expect (global.localStorage.removeItem.mock.calls).toEqual ([['jwt']]);
    });
  });
});
