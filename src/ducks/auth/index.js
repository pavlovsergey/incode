import {appName} from '../../config';
import * as axiosForTest from '../../utils/axios';
import {Record} from 'immutable';
import {SubmissionError} from 'redux-form';

export const moduleName = 'auth';

// Actions
export const SIGN_IN_SUCCESS = `${appName}/${moduleName}/SIGN_IN_SUCCESS`;
export const SIGN_IN_OUT = `${appName}/${moduleName}/SIGN_IN_OUT`;
export const SIGN_UP_MESSAGE = `${appName}/${moduleName}/SIGN_UP_MESSAGE`;

export const initialState = Record ({
  auth: false,
  message: '',
});

// Reducer
export default function reducer (state = new initialState (), action = {}) {
  const {type, payload} = action;
  switch (type) {
    case SIGN_IN_SUCCESS:
      return state.set ('auth', true);
    case SIGN_IN_OUT:
      return state.set ('auth', false);
    case SIGN_UP_MESSAGE:
      return state.set ('message', payload.message);
    default:
      return state;
  }
}

// Action Creators

export function signIn (values, dispatch, props) {
  return axiosForTest
    .axs ('http://localhost:3001/signin', values)
    .then (user => {
      window.localStorage.setItem ('jwt', user.data.token);
      dispatch ({
        type: SIGN_IN_SUCCESS,
      });
    })
    .catch (function (error) {
      if (error.response) error.message = error.response.data;
      throw new SubmissionError (
        {
          _error: {
            error,
            name: error.name,
            message: error.message,
          },
        } || {
          _error: {
            error,
            name: 'Error of  error(no error.name or error.message)',
            message: 'Sign in failed!!',
          },
        }
      );
    });
}

export function unSignIn () {
  return {
    type: SIGN_IN_OUT,
  };
}

export function signUp (values, dispatch, props) {
  return axiosForTest
    .axs ('http://localhost:3001/signup', values)
    .then (user => {
      dispatch ({
        type: SIGN_UP_MESSAGE,
        payload: {
          message: user.data,
        },
      });
    })
    .catch (function (error) {
      if (error.response) error.message = error.response.data;
      throw new SubmissionError (
        {
          _error: {
            error,
            name: error.name,
            message: error.message,
          },
        } || {
          _error: {
            error,
            name: 'Error of  error(no error.name or error.message)',
            message: 'Sign up failed!!',
          },
        }
      );
    });
}

export function verification (email, code) {
  return dispatch => {
    return axiosForTest
      .axs (`http://localhost:3001/verification?name=${email}&code=${code}`, {
        email: email,
        code: code,
      })
      .then (function (jwt) {
        jwt.data &&
          jwt.data &&
          window.localStorage.setItem ('jwt', jwt.data.token);
        dispatch ({
          type: SIGN_IN_SUCCESS,
        });
      })
      .catch (function (error) {
        window.localStorage.removeItem ('jwt');
        document.location.reload (true);
      });
  };
}
