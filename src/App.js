import React, {Component} from 'react';
import MuiPickersUtilsProvider
  from 'material-ui-pickers/utils/MuiPickersUtilsProvider';
// pick utils
import DateFnsUtils from 'material-ui-pickers/utils/date-fns-utils';

import {createBrowserHistory} from 'history';

import {Router, Route, Switch} from 'react-router-dom';
import indexRoutes from 'routes/index.jsx';

import {connect} from 'react-redux';

const hist = createBrowserHistory ();

class App extends Component {
  render () {
    const {loading} = this.props;
    return (
      <div>
        <Router history={hist}>
          <MuiPickersUtilsProvider utils={DateFnsUtils}>
            <Switch>
              {indexRoutes (loading).map ((prop, key) => {
                return (
                  <Route
                    path={prop.path}
                    component={prop.component}
                    key={key}
                  />
                );
              })}
            </Switch>
          </MuiPickersUtilsProvider>
        </Router>
      </div>
    );
  }
}
const mapStateToProps = state => {
  return {
    loading: state.auth.auth,
  };
};
export default connect (mapStateToProps) (App);
