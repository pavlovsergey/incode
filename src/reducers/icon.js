import {LOAD_SUCCESS} from '../constans';

export default function icon (iconState = '', action) {
  const {type, payload} = action;

  switch (type) {
    case LOAD_SUCCESS:
      return payload.content.icon;
    default:
      return iconState;
  }
}
