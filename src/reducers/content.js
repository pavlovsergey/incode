import {CHANGE_CATEGORY, LOAD_SUCCESS, FILTER_EXPENSES} from '../constans';

const defaultContentState = {
  filterContent: {orderCategory: []},
  category: {},
};

export default function content (contentState = defaultContentState, action) {
  const {type, payload} = action;

  switch (type) {
    case LOAD_SUCCESS:
      return {
        category: payload.content.category,
        filterContent: payload.content.filterContent,
      };
    case CHANGE_CATEGORY:
      return {
        ...payload.content,
      };

    case FILTER_EXPENSES:
      return {
        ...contentState,
        category: payload.category,
      };
    default:
      return contentState;
  }
}
