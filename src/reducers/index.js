import {combineReducers} from 'redux';
import authorization from './authorization';
import content from './content';
import expenses from './expenses';
import icon from './icon';
import loadExpenses from './loadExpenses';
import {reducer as form} from 'redux-form';

import authReducer, {moduleName as authModule} from '../ducks/auth';
export default combineReducers ({
  authorization,
  content,
  expenses,
  icon,
  loadExpenses,
  form,
  [authModule]: authReducer,
});
