import {FILTER_EXPENSES} from '../constans';

export default function loadExpenses (loadExpenses = false, action) {
  const {type} = action;

  switch (type) {
    case FILTER_EXPENSES:
      return true;

    default:
      return loadExpenses;
  }
}
