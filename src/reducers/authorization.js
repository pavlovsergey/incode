import {LOAD_START, LOAD_FAIL, LOAD_SUCCESS} from '../constans';

const defaultStateAuthorization = {
  loading: false,
  loaded: false,
  validEmailPassword: true,
};

export default function authorization (
  authorizationState = defaultStateAuthorization,
  action
) {
  const {type} = action;

  switch (type) {
    case LOAD_START:
      return {...authorizationState, loading: true};

    case LOAD_FAIL:
      return {
        ...authorizationState,
        loading: false,
        loaded: false,
        validEmailPassword: false,
      };

    case LOAD_SUCCESS:
      return {...authorizationState, loading: false, loaded: true};

    default:
      return authorizationState;
  }
}
