import {LOAD_SUCCESS, ADD_EXPENSES} from '../constans';

const defaultExpensesState = {};

export default function expenses (
  expensesState = defaultExpensesState,
  action
) {
  const {type, payload} = action;

  switch (type) {
    case LOAD_SUCCESS:
      return payload.content.expenses;

    case ADD_EXPENSES:
      return {
        ...expensesState,
        ...payload.expenses,
      };
    default:
      return expensesState;
  }
}
